# coding: utf-8
import os
import numpy as np
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
from obspy import UTCDateTime
from multiprocessing import cpu_count
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
from modules.askIRIS import askIRIS
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
# *** Client tips: IRIS for CSN stations; GEONET for NZ stations;

client = 'IRIS' #GEONET does not have response !?
# stations is a dict whom keys are networks iD which contains list of stations
stations = {'NZ':['MCHZ', 'NMHZ', 'NTVZ', 'SNVZ', 'TMVZ','BKZ', 'ETVZ',
                  'KRHZ', 'KWHZ', 'MOVZ', 'MRHZ', 'OTVZ']}


components = ['EH*', 'HH*'] # can be list of str or single str
locations = ['*'] # can be list of str or single str

datadir = '/home/geovault-06/adenanto/hikurangi/data' # directory to save the data
ncpu = 20 # number of cpu to use

for year in (2008, 2009, 2010, 2011, 2012, 2013, 2016, 2017, 2018):
    print 'Downloading year {}..'.format(year)
    start = UTCDateTime(year, 1, 1) # obspy.UTCDateTime only
    end = UTCDateTime(year+1, 1, 1) # obspy.UTCDateTime only

    askIRIS(datadir, client, stations, components, locations, start, end, ncpu)
